This is tested on stretch64 which ships singularity 2.2. I'm aware this
is illustrating a legacy workflow. But I'd prefer to stick to debians
stalbe repo.

requirements:
* this poc relyies on cahced buildserver dependencies in **~/.cache/fdroidserver**. (use makebuildserver script to pupulate this cache)
* you'll need 60gb of free diskspace for running this script. (for some reason singularity allocates the entire image-file before making it sparse)

braindump:

    apt install debootstrap singularity-container
    sudo bash makebuildcontainer

    cd .../fdroiddata
    singularity exec .../fdroid-singularity-build-poc/fd.img fdroid build org.fdroid.fdroid:16

resources:
* http://singularity.lbl.gov/archive/docs/v2-2
